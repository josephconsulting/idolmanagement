﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace IDOLManagement
{
    class Program
    {
        public static string IdolDbToFlush { get; set; }
        private static ServiceController scCFSFS = new ServiceController("MicroFocus-CFS-FS");
        private static ServiceController scFSCon = new ServiceController("MicroFocus-FileSystemConnector");

        public const string CFSLogPath = @"C:\MicroFocus\IDOLServer-11.6.0\CFS-FS\cfs\logs";
        public const string CFSActionsPath = @"C:\MicroFocus\IDOLServer-11.6.0\CFS-FS\cfs\actions";
        public const string CFSPath = @"C:\MicroFocus\IDOLServer-11.6.0\CFS-FS\cfs";

        public const string FSLogPath = @"C:\MicroFocus\IDOLServer-11.6.0\filesystemconnector\logs";
        public const string FSActionsPath = @"C:\MicroFocus\IDOLServer-11.6.0\filesystemconnector\actions";
        public const string FSPath = @"C:\MicroFocus\IDOLServer-11.6.0\filesystemconnector";

        
        static void Main(string[] args)
        {
            DeleteDBDocs(args);
            GetStatusOfCFSFS();
            StopCFSFS();
            DeleteCFSLogs();
            DeleteCFSActions();
            DeleteCFSPreAndPostIDX();
            StopFSCon();
            DeleteFSConActions();
            DeleteFSConLogs();
            DeleteFSConDB();
            StartCFSFS();
            StartFSConn();
            Console.WriteLine("operation complete");
            Console.ReadLine();
        }

        private static void DeleteCFSPreAndPostIDX()
        {
            string CFSPostIDXPath = @"C:\MicroFocus\IDOLServer-11.6.0\CFS-FS\cfs\post.idx";
            string CFSPreIDXPath = @"C:\MicroFocus\IDOLServer-11.6.0\CFS-FS\cfs\pre.idx";
            FileInfo f1 = new FileInfo(CFSPostIDXPath);
            FileInfo f2 = new FileInfo(CFSPreIDXPath);
            List<FileInfo> IdxToDelete = new List<FileInfo>();
            IdxToDelete.Add(f1);
            IdxToDelete.Add(f2);
            IdxToDelete.ForEach(deleterFiles);
        }

        private static void StartFSConn()
        {
            if (scFSCon.Status == ServiceControllerStatus.Stopped)
            {
                scFSCon.Start();
                scFSCon.WaitForStatus(ServiceControllerStatus.Running);
                Console.WriteLine("File System Connector is Running");
            }
        }

        private static void StartCFSFS()
        {
            if (scCFSFS.Status == ServiceControllerStatus.Stopped)
            {
                scCFSFS.Start();
                scCFSFS.WaitForStatus(ServiceControllerStatus.Running);
                Console.WriteLine("File System Connector Framework Server is now running");
            }
        }

        private static void DeleteFSConDB()
        {
            string DirectoryPath = @"C:\MicroFocus\IDOLServer-11.6.0\filesystemconnector";
            DirectoryInfo dir = new DirectoryInfo(DirectoryPath);
            dir.GetFiles("*.db").ToList().ForEach(deleterFiles);
            Console.WriteLine("Delete Completed.");
        }

        private static Action<FileInfo> deleterFiles = f =>
        {
            if (System.IO.File.Exists(f.FullName))
            {
                if (!IsFileLocked(f))
                {
                    System.IO.File.Delete(f.FullName);
                    Console.WriteLine($"Deleting {f.FullName}");
                }
                else
                {
                    Console.WriteLine($"Unable to delete as {f.FullName} is locked");
                }
            }
            else
            {
                Console.WriteLine($"File does not exist {f.FullName}");
            }
        };
        private static Action<DirectoryInfo> deleterDir = d =>
        {
            System.IO.Directory.Delete(d.FullName,true);
            Console.WriteLine($"Deleting {d.FullName}");
        };
        private static void DeleteFSConLogs()
        {
            string DirectoryPath = @"C:\MicroFocus\IDOLServer-11.6.0\filesystemconnector\logs";
            DeleteFilesAndFolders(DirectoryPath);
        }

        private static void DeleteFSConActions()
        {
            string DirectoryPath = @"C:\MicroFocus\IDOLServer-11.6.0\filesystemconnector\actions";
            DeleteFilesAndFolders(DirectoryPath);
        }

        private static void StopFSCon()
        {
            if (scFSCon.Status == ServiceControllerStatus.Running)
            {
                //
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create("http://localhost:17002/action=Stop");
                using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())
                {
                    if (response.StatusCode == HttpStatusCode.OK)
                    {
                        Console.WriteLine("Stopping File System Connector");
                        scFSCon.WaitForStatus(ServiceControllerStatus.Stopped);
                        Console.WriteLine("Stopped File System Connector");
                    }
                }
                
            }
        }

        private static void DeleteCFSActions()
        {
            string DirectoryPath = @"C:\MicroFocus\IDOLServer-11.6.0\CFS-FS\cfs\actions";
            DeleteFilesAndFolders(DirectoryPath);
        }

        private static void DeleteCFSLogs()
        {
            string DirectoryPath = @"C:\MicroFocus\IDOLServer-11.6.0\CFS-FS\cfs\logs";
            DeleteFilesAndFolders(DirectoryPath);
        }

        private static void DeleteFilesAndFolders(string DirectoryPath)
        {
            if (DirectoryPath.CompareTo(CFSActionsPath) == 0 || DirectoryPath.CompareTo(CFSLogPath) == 0 || DirectoryPath.CompareTo(CFSPath) == 0)
            {
                if (!scCFSFS.Status.Equals(ServiceControllerStatus.Stopped))
                {
                    scCFSFS.WaitForStatus(ServiceControllerStatus.Stopped);
                }
            }
            if (DirectoryPath.CompareTo(FSActionsPath) == 0 || DirectoryPath.CompareTo(FSLogPath) == 0 || DirectoryPath.CompareTo(FSPath) == 0)
            {
                if (!scFSCon.Status.Equals(ServiceControllerStatus.Stopped))
                {
                    scFSCon.WaitForStatus(ServiceControllerStatus.Stopped);
                }
            }
            System.IO.DirectoryInfo logsCfs = new DirectoryInfo(DirectoryPath);
            logsCfs.GetFiles("*.logs").ToList().ForEach(deleterFiles);
            logsCfs.GetDirectories().ToList().ForEach(deleterDir);
                
            
        }
        
        private static void StopCFSFS()
        {
            if (scCFSFS.Status == ServiceControllerStatus.Running)
            {
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create("http://localhost:17000/action=Stop");
                using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())
                {
                    if (response.StatusCode == HttpStatusCode.OK)
                    {
                        Console.WriteLine("Stopping CFS for File System");
                        scFSCon.WaitForStatus(ServiceControllerStatus.Stopped);
                        Console.WriteLine("Stopped CFS for File System");
                    }
                }
                
            }
        }
        private static bool IsFileLocked(FileInfo file)
        {
            FileStream stream = null;

            try
            {
                stream = file.Open(FileMode.Open, FileAccess.ReadWrite, FileShare.None);
            }
            catch (IOException)
            {
                //the file is unavailable because it is:
                //still being written to
                //or being processed by another thread
                //or does not exist (has already been processed)
                return true;
            }
            finally
            {
                if (stream != null)
                    stream.Close();
            }

            //file is not locked
            return false;
        }
        private static void DeleteDBDocs(string[] args)
        {
            if (args.Length == 0)
            {
                IdolDbToFlush = "CTGOV";
            }

            string url = $"http://localhost:9101/DREDELDBASE?DREDbName={IdolDbToFlush}";
            HttpWebRequest aciRequest = (HttpWebRequest)WebRequest.Create(url);
            using (HttpWebResponse response = (HttpWebResponse)aciRequest.GetResponse())
            {
                if (response.StatusCode == HttpStatusCode.OK)
                {
                    Console.WriteLine("Documents deleted");
                }
            }
        }

        private static void GetStatusOfCFSFS()
        {
            Console.WriteLine("The Service controller is currently set to {0}", scCFSFS.Status.ToString());
        }
    }
}
